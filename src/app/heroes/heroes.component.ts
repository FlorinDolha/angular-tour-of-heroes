import { Component, OnInit, OnDestroy } from '@angular/core';
import { Hero } from '../hero';
import { Character } from '../character';
import { Utils } from '../Utils';
import { HeroService } from '../services/hero.service';
import { SignalRService } from '../services/signal-r.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit, OnDestroy {
  heroes: Hero[];
  subscription: Subscription
  constructor(public heroService: HeroService, private signalRService: SignalRService) {
  }

  ngOnInit(): void {
    this.subscription = this.heroService.heroes.subscribe(heroes => this.heroes = heroes);
//    this.getHeroes()
    // this.signalRService.startConnection()
    // this.signalRService.listenToHeroesChanged()
    // this.signalRService.heroesObserver.subscribe(data => {
    //   this.heroService.heroes = data as any
    // })
  }

  add(name: string): void {
    this.heroService.add(name)
  }

  delete(hero: Hero): void{
    this.heroService.delete(hero);
  }

  ngOnDestroy(): void{
    this.subscription.unsubscribe();
  }

  identify(index, item){
    return item.id; 
  }
}