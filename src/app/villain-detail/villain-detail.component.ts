import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Villain } from '../villain';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { VillainService }  from '../services/villain.service';
import {ServiceMapper} from '../serviceMapper';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-villain-detail',
  templateUrl: './villain-detail.component.html',
  styleUrls: ['./villain-detail.component.css']
})
export class VillainDetailComponent implements OnInit, OnDestroy {
  villain: Villain;
  public service = ServiceMapper.villains;
  private subscriptions: Subscription[]

  constructor(
    private route: ActivatedRoute,
    private villainService: VillainService,
    private location: Location) { 
    
  }

  ngOnInit(): void {
    this.getVillain();
  }
  
  getVillain(): void {
    this.route.params.subscribe(params => {
          this.villainService.getVillain(params.id)
          .subscribe(villain => this.villain = villain as Villain);
    })
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.villainService.updateVillain(this.villain);
    this.goBack();
  }

  ngOnDestroy(): void {
    
  }
}