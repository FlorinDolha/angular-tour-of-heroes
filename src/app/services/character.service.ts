import { Injectable, Inject, Optional } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { MessageService } from './message.service';
import { Observable, of } from 'rxjs';
import { Character } from '../character';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export abstract class CharacterService {
  protected httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(
    protected http: HttpClient,
    protected messageService: MessageService) {
  }

  getCharacters(url: string): Observable<Character[]> {
    return this.http.get<Character[]>(url)
      .pipe(
        tap(_ => this.log(`fetched characters`)),
        catchError(this.handleError<Character[]>('getCharacters', []))
      );
  }

  getCharacter(id: number, url: string): Observable<Character> {
    url = `${url}/${id}`;
    return this.http.get<Character>(url)
      .pipe(
        tap(_ => this.log(`fetched character id=${id}`)),
        catchError(this.handleError<Character>(`getCharacter id=${id}`))
      );
  }

  updateCharacter(character: Character, url: string): Observable<Character[]>{
    return this.http.put(url, character, this.httpOptions)
      .pipe(
        tap(_ => this.log(`updated character id=${character.id}`)),
        catchError(this.handleError<any>('updateCharacter'))
      )
  }

  addCharacter(character: Character, url: string): Observable<Character> {
    return this.http.post<Character>(url, character, this.httpOptions)
      .pipe(
        tap((newCharacter: Character) => this.log(`added character w/ id=${newCharacter.id}`)),
        catchError(this.handleError<Character>('addCharacter'))
      );
  }

  deleteCharacter(character: Character, url: string): Observable<Character>{
    const id = character.id;
    url = `${url}/${id}`;

    return this.http.delete<Character>(url, this.httpOptions)
      .pipe(
        tap(_ => this.log(`deleted character id=${id}`)),
        catchError(this.handleError<Character>('deleteCharacter'))
      );
  }

  searchCharacters(term: string, url: string): Observable<Character[]> {
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<Character[]>(`${url}/?name=${term}`)
      .pipe(
        tap(x => x.length ?
          this.log(`found characters matching "${term}"`) :
          this.log(`no characters matching "${term}"`)),
        catchError(this.handleError<Character[]>('searchCharacters', []))
      );
  }

  public handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error)
      this.log(`${operation} failed: ${error.message}`)
      return of(result)
    }
  }

  public log(message: string) {
    this.messageService.add(`CharacterService: ${message}`);
  }
}