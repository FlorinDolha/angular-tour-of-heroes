import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr'
import { Subject, Observable } from 'rxjs';
import { Hero } from '../hero';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {
  public heroesObserver = new Subject<Hero>()
  private hubConnection: signalR.HubConnection

  constructor() { }

  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
            .withUrl('https://localhost:5001/heroes')
            .build()

    this.hubConnection.start()
                      .then(() => console.log('Connection started'))
                      .catch(error => console.log(`Error while starting the connection: ${error}`))
  }

  public listenToHeroesChanged() : void {
    this.hubConnection.on('heroesChanged', (data) => {
      this.heroesObserver.next(data)
    })
  }
}