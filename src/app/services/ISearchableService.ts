import { Observable } from 'rxjs';
import { Character } from '../character';

export interface ISearchableService{
    search(term: string) : Observable<Character[]>;
}