import { Injectable } from '@angular/core'
import { CharacterService } from './character.service'
import { Hero } from '../hero';
import { BehaviorSubject, Observable } from 'rxjs';
import { ISearchableService } from './ISearchableService';
import { Character } from '../character';

@Injectable({
  providedIn: 'root'
})

export class HeroService implements ISearchableService {
  url: string = 'https://localhost:5001/api/heroes';

  heroes = new BehaviorSubject<Hero[]>([])
  constructor(private characterService: CharacterService) {
    this.getHeroes()
  } 

  search(term: string) : Observable<Character[]> {
    return this.characterService.searchCharacters(term, this.url);
  }
  
  add(name: string) {
    name = name.trim();
    if (!name) { return; }
    this.characterService.addCharacter({ name } as unknown as Hero, this.url)
        .subscribe(hero => this.heroes.next([...this.heroes.getValue(), hero as Hero]))
  }

  delete(hero: Hero) {
    this.characterService.deleteCharacter(hero, this.url)
      .subscribe(() => {
        this.heroes.next(this.heroes.getValue().filter(filteredHero => hero !== filteredHero));
      })
  }
  
  getHeroes(): void{
    this.characterService.getCharacters(this.url)
        .subscribe((heroes: Hero[]) => this.heroes.next(heroes))
  }

  getCharacter(id: number) : Observable<Character> {
    return this.characterService.getCharacter(id, this.url);
  }

  updateCharacter(hero: Hero) {
    const reducer = (accumulator: Hero[], currentValue: Hero) => {
      console.log(currentValue);
      if (currentValue.id == hero.id){
        return [...accumulator, hero];
      }
      return [...accumulator, currentValue];
    }

    this.characterService
      .updateCharacter(hero, this.url)
      .subscribe(heroes => {
        this.heroes.next(heroes as Hero[]);
      });
  }
}