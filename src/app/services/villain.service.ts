import { Injectable } from '@angular/core';
import { CharacterService } from './character.service';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { Villain } from '../villain';
import { tap, catchError } from 'rxjs/operators';
import { ISearchableService } from './ISearchableService';

@Injectable({
  providedIn: 'root'
})
export class VillainService implements ISearchableService {
  url: string = 'https://localhost:5001/api/villains';
  villains = new BehaviorSubject<Villain[]>([]);
  constructor(private http: HttpClient, private characterService: CharacterService) {
    this.getVillains();
  }

  getVillainsByHero(id: number): Observable<Villain[]> {
    const url = `${this.url}/id_hero/${id}`;
    return this.http.get<Villain[]>(url)
      .pipe(
        tap(_ => this.characterService.log(`fetched villains of hero id=${id}`)),
        catchError(this.characterService.handleError<Villain[]>(`getVillainsByHero id=${id}`))
      );
  }

  updateVillain(villain: Villain){
    this.characterService.updateCharacter(villain, this.url)
      .subscribe(characters => this.villains.next(characters as Villain[]))
  }

  getVillains(){
    this.characterService.getCharacters(this.url)
      .subscribe(characters => this.villains.next(characters as Villain[]));
  }

  getVillain(id: number) {
    return this.characterService.getCharacter(id, this.url);
  }

  addVillain(name: string, id_hero: number) {
    name = name.trim();
    if (!name) { return; }
    this.characterService.addCharacter({ name: name, id_hero: id_hero } as unknown as Villain, this.url)
        .subscribe(villain => this.villains.next([...this.villains.getValue(), villain as Villain]))
  }
  
  deleteVillain(villain: Villain) {
    this.characterService.deleteCharacter(villain, this.url)
        .subscribe(() => this.villains.next(this.villains.getValue().filter(filteredVillain => villain !== filteredVillain)))
  }

  search(term: string) {
    return this.characterService.searchCharacters(term, this.url);
  }
}