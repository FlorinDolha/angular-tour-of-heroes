import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../services/hero.service';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {
  @Input() hero: Hero;
  @Output() heroChange: EventEmitter<Hero>;
  constructor(private heroService: HeroService) { }

  ngOnInit(): void {
  }

  delete(): void{
    this.heroService.delete(this.hero);
    this.heroChange.emit(this.hero);
  }
}
