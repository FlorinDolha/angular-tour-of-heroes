import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Hero } from '../hero';
import { Villain } from '../villain';
import { VillainService } from '../services/villain.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-villains',
  templateUrl: './villains.component.html',
  styleUrls: ['./villains.component.css']
})
export class VillainsComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  @Input() hero: Hero;
  villains : Villain[];

  constructor(private villainService: VillainService) {
  }

  ngOnInit(): void {
    this.subscription = this.villainService.villains.subscribe(villains => {
      this.villains = villains.filter(villain => villain.id_hero == this.hero.id)
    })
  }

  add(name: string): void {
    this.villainService.addVillain(name, this.hero.id);
  }

  delete(villain: Villain): void{
    this.villainService.deleteVillain(villain);
  }

  identify(index, item){
    return item.id; 
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}