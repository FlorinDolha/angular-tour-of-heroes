import { HeroService } from './services/hero.service';
import { VillainService } from './services/villain.service';

export const ServiceMapper = {
    heroes: HeroService,
    villains: VillainService
}