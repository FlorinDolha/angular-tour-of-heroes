import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VillainTooltipComponent } from './villain-tooltip.component';

describe('VillainTooltipComponent', () => {
  let component: VillainTooltipComponent;
  let fixture: ComponentFixture<VillainTooltipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VillainTooltipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VillainTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
