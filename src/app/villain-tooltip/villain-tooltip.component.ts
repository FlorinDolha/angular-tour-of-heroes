import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../services/hero.service';
import { VillainService } from '../services/villain.service';
import { Villain } from '../villain';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-villain-tooltip',
  templateUrl: './villain-tooltip.component.html',
  styleUrls: ['./villain-tooltip.component.css']
})
export class VillainTooltipComponent implements OnInit, OnDestroy {
  villains: Villain[];
  @Input() hero: Hero;
  private subscription: Subscription;
  constructor(public villainService: VillainService) { }

  ngOnInit(): void {
    this.subscription = this.villainService.villains.subscribe(villains => this.villains = villains.filter(villain => villain.id_hero == this.hero.id));
  }

  identify(index, item){
    return item.id; 
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
