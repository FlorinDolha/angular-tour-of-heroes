export class Utils{
    
  public static convertArray<T1, T2>(array: T1[]) : T2[]{
    return array.map(item => item as unknown as T2);
  }
}