import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { Character } from '../character';
import { Utils } from '../Utils';
import { of, Observable } from 'rxjs';
import { SignalRService } from '../services/signal-r.service';
import { HeroService } from '../services/hero.service';
import {ServiceMapper} from "../serviceMapper";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public heroes: Hero[]
  public service = ServiceMapper.heroes
  constructor(public heroService: HeroService, private signalRService: SignalRService) {
    this.heroService.heroes.subscribe(heroes => this.heroes = heroes)
   }

  ngOnInit(): void {
    // this.getHeroes()
    // // this.signalRService.startConnection()
    // // this.signalRService.listenToHeroesChanged()
    // // this.signalRService.heroesObserver.subscribe(data => {
    // //   this.heroService.heroes = data as any
    // // })
  }

  // getHeroes(): void {
  //   this.heroService.getCharacters()
  //     .subscribe(heroes => {
  //       this.heroService.heroes = Utils.convertArray<Character, Hero>(heroes).slice(0, 4)
  //     });
  // }

  identify(index, item){
    return item.id; 
  }
}