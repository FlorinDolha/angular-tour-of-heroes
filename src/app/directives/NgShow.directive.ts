import { Input, Directive, TemplateRef, ViewContainerRef, ElementRef, AfterViewInit } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { debounceTime } from 'rxjs/operators';

@Directive({ selector: '[ngShow]'})
export class NgShowDirective {
    private hasView = false;

    constructor(
        private element: ElementRef,
        private template: TemplateRef<any>,
        private view: ViewContainerRef){
    }

    public createView(): Promise<ViewContainerRef> {
        return new Promise((resolve)=>{
            if (!this.hasView){
                this.view.createEmbeddedView(this.template);
                this.hasView = true;
            }
            resolve();
        })
    }

    @Input() set ngShow(condition: boolean) {
        //this.element.nativeElement.style.display = condition ? 'block' : 'none';
        this.createView().then(() => this.element.nativeElement.style.display = condition ? 'block' : 'none')
     //   this.el.nativeElement.style.dispay = condition ? 'block' : 'none';
    }
}