import { Component, OnInit, Input, Injector } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Character } from '../character';
import { ISearchableService } from '../services/ISearchableService';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  characters$: Character[];
  protected searchTerms = new Subject<string>();
  @Input() url: string
  @Input() service: ISearchableService

  search(term: string): void {
    this.searchTerms.next(term);
  }

  constructor(private injector: Injector) {
    
  }

  ngOnInit(): void {
    if (!this.service){
      throw new Error("service must be defined");
    }
    this.service = this.injector.get(this.service)

    this.searchTerms
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((term: string) => this.service.search(term)),
      )
      .subscribe(character => this.characters$ = character);
  }

  identify(index, item){
    return item.id; 
  }
}