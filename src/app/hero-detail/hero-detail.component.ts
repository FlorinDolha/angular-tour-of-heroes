import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Hero } from '../hero';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HeroService } from '../services/hero.service';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {
  hero: Hero;

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location) { 
  }

  ngOnInit(): void {
    this.getHero()
  }
  
  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.heroService.getCharacter(id)
      .subscribe(hero => {
        this.hero = hero as Hero
        if (this.hero === undefined || this.hero === null){
          this.goBack();
        }
      });
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.heroService.updateCharacter(this.hero);
    this.goBack();
  }
}